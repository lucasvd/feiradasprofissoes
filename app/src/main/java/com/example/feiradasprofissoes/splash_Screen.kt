package com.example.feiradasprofissoes

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity

class splash_Screen : AppCompatActivity() {

    private var mHandler: Handler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val updateUI: () -> Unit = {
            val it = Intent(this, tela_Login::class.java)
            startActivity(it)
            finish()
        }

        mHandler.postDelayed(updateUI, 4000)

    }
}
