package com.example.feiradasprofissoes

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_cadastro.*
import kotlinx.android.synthetic.main.activity_login.*

class tela_Login : AppCompatActivity() {
    //Paramentros
    private lateinit var mbotaoLogar : Button
    private lateinit var mLinkTelaCadastro : TextView
    private lateinit var mEditText : EditText
    private lateinit var mEditText2: EditText
    private lateinit var autenticacao: FirebaseAuth
    private lateinit var usuario: Usuario

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //Views da Tela Activity Main

        mLinkTelaCadastro = findViewById(R.id.link_Tela_Cadastro)
        mbotaoLogar = findViewById(R.id.botao_logar)
        mEditText =  findViewById(R.id.campo_email1)
        mEditText2 =  findViewById(R.id.campo_senha1)


        // Botão para realizar o Login
        mbotaoLogar.setOnClickListener{

            // verificação das caixas em branco
            if (mEditText!!.text.toString() != "" && mEditText2!!.text.toString() != "") {
                usuario = Usuario()

                usuario!!.email = mEditText!!.text.toString()
                usuario!!.senha = mEditText2!!.text.toString()
                // chamda do metodo validação login
                validarLogin()


            } else {
                Toast.makeText(this@tela_Login, "Preencha os campos de E-mail e senha", Toast.LENGTH_SHORT).show()
            }

        }
        // Chamada para a tela de cadastro
        mLinkTelaCadastro.setOnClickListener{

            val intent = Intent(this, tela_Cadastro::class.java)
            startActivity(intent)

        }

    }
    // metodo validação de login
    private fun validarLogin() {
        autenticacao = ConfiguracaoFirebase.firebaseAuth
        autenticacao!!.signInWithEmailAndPassword(usuario!!.email.toString(), usuario!!.senha.toString()).addOnCompleteListener { task ->
            if (task.isSuccessful) {

                Toast.makeText(this, "Login efetuado com sucesso!", Toast.LENGTH_SHORT).show()
                abrirTelaMain()

            } else {
                Toast.makeText(this, "Usuário ou senha inválidos! Tente novamente!", Toast.LENGTH_SHORT).show()
            }
        }
    }
    //função para abrir o main
    private fun abrirTelaMain() {
        val intent = Intent(this, tela_Main::class.java)
        startActivity(intent)

    }



    }

