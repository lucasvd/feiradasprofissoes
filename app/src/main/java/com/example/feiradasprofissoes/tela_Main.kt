package com.example.feiradasprofissoes

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView


class tela_Main : AppCompatActivity() {

    private lateinit var mTextView: TextView
    private lateinit var mTextView2: TextView
    private lateinit var mTextView3: TextView
    private lateinit var mTextView4: TextView
    private lateinit var mButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fun SharedPreferences(context: Context){
            val PREFS_NAME = "kotlincodes"
            val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        }




        mTextView = findViewById(R.id.textView2)
        mTextView2 = findViewById(R.id.textView)
        mTextView3 = findViewById(R.id.textView3)
        mTextView4 = findViewById(R.id.textView4)
        mButton = findViewById(R.id.button)
        mButton.setOnClickListener{

            val intent = Intent(this, tela_Sorteio::class.java)
            startActivity(intent)

        }

        mTextView.setOnClickListener{

            val intent = Intent(this, info_Curso_Ads::class.java)
            startActivity(intent)

        }
        mTextView2.setOnClickListener{
            val intent = Intent(this, info_Curso_CC::class.java)
            startActivity(intent)
        }
        mTextView3.setOnClickListener{
            val intent = Intent(this, info_Curso_EC::class.java)
            startActivity(intent)
        }
        mTextView4.setOnClickListener{
            val intent = Intent(this, info_Curso_EE::class.java)
            startActivity(intent)
        }


    }
}

