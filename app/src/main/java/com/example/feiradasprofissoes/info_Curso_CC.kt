package com.example.feiradasprofissoes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView

class info_Curso_CC : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info__curso__cc)

        val myWebView: WebView = findViewById(R.id.unifor_cc)
        myWebView.loadUrl("https://www.unifor.br/web/graduacao/ciencia-da-computacao")

        myWebView.settings.javaScriptEnabled = true
    }
}
