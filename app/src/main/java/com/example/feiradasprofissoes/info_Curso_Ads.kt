package com.example.feiradasprofissoes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView

class info_Curso_Ads : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info__curso__ads)

        val myWebView: WebView = findViewById(R.id.unifor_ads)
        myWebView.loadUrl("https://www.unifor.br/web/graduacao/analise-e-desenvolvimento-de-sistema")

        myWebView.settings.javaScriptEnabled = true
    }
}
