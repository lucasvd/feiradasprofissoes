package com.example.feiradasprofissoes


import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog

import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_sorteio.*
import java.util.jar.Manifest


class tela_Sorteio : AppCompatActivity() {

    var fileUri: Uri? = null
    val shareIntent = Intent(Intent.ACTION_SEND)





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sorteio)



        //botao para tirar foto
        takePhoto.setOnClickListener {
            askCameraPermission()
        }

    }


    //carregar camera via intent
    private fun launchCamera() {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        fileUri = contentResolver
            .insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(intent.resolveActivity(packageManager) != null) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION
                        or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, AppConstants.TAKE_PHOTO_REQUEST)
        }
    }

    //perguntar por permissao para tirar foto
    fun askCameraPermission(){
        Dexter.withActivity(this)
            .withPermissions(
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                    if(report.areAllPermissionsGranted()){
                        //se permissoes concedidas, carregar camera
                        launchCamera()
                    }else{
                        Toast.makeText(this@tela_Sorteio, "Todas as permissoes precisam ser garantias para tirar fotos", Toast.LENGTH_LONG).show()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {/* ... */
                    //mostrar caixa de dialogo com opçoes de permissao
                    AlertDialog.Builder(this@tela_Sorteio)
                        .setTitle(
                            "Erro de permissão!")
                        .setMessage(
                            "Por favor, conceda permissão para poder tirar fotos com a camera")
                        .setNegativeButton(
                            android.R.string.cancel,
                            { dialog, _ ->
                                dialog.dismiss()
                                token?.cancelPermissionRequest()
                            })
                        .setPositiveButton(android.R.string.ok,
                            { dialog, _ ->
                                dialog.dismiss()
                                token?.continuePermissionRequest()
                            })
                        .setOnDismissListener({
                            token?.cancelPermissionRequest() })
                        .show()
                }

            }).check()

    }

    //funcao chamada quando a foto for tirada
    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        if (resultCode == Activity.RESULT_OK
            && requestCode == AppConstants.TAKE_PHOTO_REQUEST) {
            //pegar foto da camera
            //carregar a foto no imageview
            imageView.setImageURI(fileUri)
        }
   //intents para chamar api do instagram
        shareIntent.type = "image/*"
        shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);

        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        shareIntent.putExtra(Intent.EXTRA_TEXT, "#CCTUNIFOR")
        shareIntent.setPackage("com.instagram.android")
        startActivity(Intent.createChooser(shareIntent, "Compartilhar para"));
    }


}
