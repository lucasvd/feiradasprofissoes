package com.example.feiradasprofissoes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView

class info_Curso_EE : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info__curso__ee)


        val myWebView: WebView = findViewById(R.id.unifor_ee)
        myWebView.loadUrl("https://www.unifor.br/web/graduacao/engenharia-eletrica")

        myWebView.settings.javaScriptEnabled = true
    }
}
