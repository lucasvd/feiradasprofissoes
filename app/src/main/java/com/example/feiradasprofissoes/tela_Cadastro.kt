package com.example.feiradasprofissoes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.graphics.TypefaceCompatApi26Impl
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_cadastro.*
import com.google.firebase.database.IgnoreExtraProperties



class tela_Cadastro : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro)

        mcadastrar.setOnClickListener{
            cadastrarUsuario()
        }
    }

    private fun cadastrarUsuario(){
        val email = campo_email_cadastro.text.toString()
        val pass = campo_senha_cadastro.text.toString()
        val nome = campo_Nome_Cadastro.text.toString()
        val cidade = campo_cidade_cadastro.text.toString()
        val escola = campo_Nome_Escola.text.toString()

        if(email.isEmpty() || pass.isEmpty() || nome.isEmpty() ||cidade.isEmpty() || escola.isEmpty()){
            Toast.makeText(this, "Por favor, preencha todos os campos!", Toast.LENGTH_SHORT).show()
            return
        }

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, pass)
            .addOnCompleteListener{
                if(!it.isSuccessful) return@addOnCompleteListener
                salvar()
                Toast.makeText(this, "Usuario Criado!", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{
                Toast.makeText(this, "Por favor, insira seu email ou senha validos!", Toast.LENGTH_SHORT).show()
            }

    }

    private fun salvar(){
        val uid = FirebaseAuth.getInstance().uid
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(uid.toString(), campo_Nome_Cadastro.text.toString(), campo_cidade_cadastro.text.toString(), campo_Nome_Escola.text.toString(), campo_email_cadastro.text.toString())

        ref.setValue(user)
            .addOnSuccessListener {
            }
    }
}

class User(val id:String, val nome: String, val cidade: String, val escola: String, val email: String)

