package com.example.feiradasprofissoes

class Usuario {
    var id: Int? = 0
    var email: String? = null
    var senha: String? = null
    var nome: String? = null
    var cidade: String? = null
    var escola: String? = null
    var tipoEscola: String? = null
}
